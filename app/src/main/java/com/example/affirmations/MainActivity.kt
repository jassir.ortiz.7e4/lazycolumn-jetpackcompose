/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations

// URL https://developer.android.com/codelabs/basic-android-kotlin-compose-woof-animation?continue=https%3A%2F%2Fdeveloper.android.com%2Fcourses%2Fpathways%2Fandroid-basics-compose-unit-3-pathway-3%23codelab-https%3A%2F%2Fdeveloper.android.com%2Fcodelabs%2Fbasic-android-kotlin-compose-woof-animation#3

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandMore

import android.os.Bundle
import android.telecom.Call.Details
import android.util.Log
import android.webkit.WebView.FindListener
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.StringRes
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.expandIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.Shapes

private const val TAG = "MainActivity"

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate Called")
        setContent {
            AffirmationApp()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart Called ")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume Called ")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause Called ")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop Called ")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy Called ")
    }


}

@Composable
fun AffirmationApp() {
    // TODO 4. Apply Theme and affirmation list
    AffirmationsTheme() {
        val affirmation = Datasource().loadAffirmations()
        AffirmationList(affirmationList = affirmation)
    }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    // TODO 3. Wrap affirmation card in a lazy column
  LazyColumn(
      modifier = Modifier.fillMaxHeight()
  ){
      items(affirmationList.count()) { index ->
          AffirmationCard(affirmation = affirmationList[index])
      }
  }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    // TODO 1. Your card UI
    var expanded by remember { mutableStateOf(false) }

        Card(
        shape = RoundedCornerShape(10.dp),
        elevation = 10.dp,
        modifier = Modifier
            .padding(10.dp)
            .fillMaxHeight()
            .shadow(10.dp)
    ) {
        Column(
            modifier = Modifier.animateContentSize(
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = stringResource(
                        id = affirmation.stringResourceId
                    ),
                    modifier = Modifier
                        .size(100.dp)
                        .padding(8.dp)
                        .clip(CutCornerShape(20))
                )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    modifier = Modifier
                        .weight(1f)
                )
                DetailsItemButton(expanded = expanded, onClick = { expanded = !expanded })
            }
            if (expanded) DescriptionsInCard(desCard = affirmation.stringResourceId)
        }
    }
}

@Composable
private fun DetailsItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector =
            if (expanded){
                Icons.Filled.ExpandLess
            } else{
                Icons.Filled.ExpandMore
            },
            tint = MaterialTheme.colors.secondary,
            contentDescription = "stringResource(R.string.expand_button_content_description)"
        )
    }
}
@Composable
fun DescriptionsInCard(
    @StringRes desCard: Int, modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.padding(
            start = 16.dp,
            top = 8.dp,
            bottom = 16.dp,
            end = 16.dp
        )
    ) {
        Text(
            text = stringResource(id = desCard)
        )
    }
}


@Preview
@Composable
private fun AffirmationCardPreview() {
    // TODO 2. Preview your card
    AffirmationList(affirmationList = Datasource().loadAffirmations())
}

